FROM adoptopenjdk:11-jre-hotspot
EXPOSE 9090
COPY build/libs/*.jar small_square.jar
ENTRYPOINT ["java", "-jar", "/small_square.jar"]
