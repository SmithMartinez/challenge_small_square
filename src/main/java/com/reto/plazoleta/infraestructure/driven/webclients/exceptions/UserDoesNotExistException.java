package com.reto.plazoleta.infraestructure.driven.webclients.exceptions;

public class UserDoesNotExistException extends RuntimeException{

    public UserDoesNotExistException(String message) {
        super(message);
    }
}
