package com.reto.plazoleta.infraestructure.driven.jpa.repository;


import com.reto.plazoleta.infraestructure.driven.jpa.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoryRepository extends JpaRepository<CategoryEntity, Long> {

}
