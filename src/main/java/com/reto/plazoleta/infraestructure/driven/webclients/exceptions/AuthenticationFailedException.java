package com.reto.plazoleta.infraestructure.driven.webclients.exceptions;

public class AuthenticationFailedException extends RuntimeException{

    public AuthenticationFailedException() {
        super("Failed to parse JWT token");
    }
}
