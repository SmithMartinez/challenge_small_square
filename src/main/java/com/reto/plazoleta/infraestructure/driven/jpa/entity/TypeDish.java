package com.reto.plazoleta.infraestructure.driven.jpa.entity;

public enum TypeDish {
    CARNE,
    SOPAS,
    POSTRE_FLAN,
    POSTRE_HELADO
}
