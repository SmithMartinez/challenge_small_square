package com.reto.plazoleta.infraestructure.driven.jpa.repository;

import com.reto.plazoleta.infraestructure.driven.jpa.entity.OrderDishEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderDishRepository extends JpaRepository<OrderDishEntity, Long> {
}
