package com.reto.plazoleta.infraestructure.driven.jpa.entity;

public enum StatusOrder {
    PENDIENTE,
    EN_PREPARACION,
    CANCELADO,
    LISTO,
    ENTREGADO
}
