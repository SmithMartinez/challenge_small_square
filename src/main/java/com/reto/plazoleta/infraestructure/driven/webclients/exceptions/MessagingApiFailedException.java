package com.reto.plazoleta.infraestructure.driven.webclients.exceptions;

public class MessagingApiFailedException extends RuntimeException{

    public MessagingApiFailedException() {
        super("Something went wrong when sending the request");
    }
}
