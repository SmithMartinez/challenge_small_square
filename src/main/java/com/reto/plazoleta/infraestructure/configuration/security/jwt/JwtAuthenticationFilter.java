package com.reto.plazoleta.infraestructure.configuration.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reto.plazoleta.infraestructure.exceptionhandler.ExceptionResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@RequiredArgsConstructor
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String TOKEN_HEADER_KEY = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";
    private final JwtProvider jwtProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String bearerToken = request.getHeader(TOKEN_HEADER_KEY);
        if(bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX) &&
                SecurityContextHolder.getContext().getAuthentication() == null) {
            String token = bearerToken.replace(TOKEN_PREFIX, "").trim();
            if(!jwtProvider.isValidToken(token)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.getWriter().write(new ObjectMapper()
                        .writeValueAsString(Collections.
                                singletonMap("message", ExceptionResponse.TOKEN_INVALID.getMessage())));
                return;
            }
            UsernamePasswordAuthenticationToken auth = jwtProvider.getAuthentication(token);
            auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        filterChain.doFilter(request, response);
    }
}
