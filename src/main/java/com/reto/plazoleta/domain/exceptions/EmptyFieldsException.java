package com.reto.plazoleta.domain.exceptions;

public class EmptyFieldsException extends RuntimeException {

    public EmptyFieldsException() {
        super();
    }
}
