package com.reto.plazoleta.domain.exceptions;

public class OrderNotExistsException extends RuntimeException{

    public OrderNotExistsException() {
        super();
    }
}
