package com.reto.plazoleta.domain.usecase;

import com.reto.plazoleta.domain.api.IAdminServicePort;
import com.reto.plazoleta.domain.exceptions.InvalidDataException;
import com.reto.plazoleta.domain.spi.clients.IUserGateway;
import com.reto.plazoleta.domain.model.RestaurantModel;
import com.reto.plazoleta.domain.spi.persistence.IRestaurantPersistencePort;
import com.reto.plazoleta.infraestructure.driven.webclients.dto.request.UserDto;
import org.springframework.security.access.AccessDeniedException;

public class AdminUseCase implements IAdminServicePort {

    private static final String ACCESS_DENIED_MESSAGE = "The user id does not have the required role to use this action";
    private static final String ROLE_OWNER = "PROPIETARIO";
    private final IRestaurantPersistencePort restaurantPersistencePort;
    private final IUserGateway userGateway;

    public AdminUseCase(IRestaurantPersistencePort restaurantPersistencePort, IUserGateway userGateway) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.userGateway = userGateway;
    }

    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel, String tokenWithBearerPrefix) {
        if(restaurantModel.isContainsOnlyNumbersInTheRestaurantName()) {
            throw new InvalidDataException();
        }
        restaurantModel.validateFieldsEmpty();
        restaurantModel.validaPhoneFormat();
        UserDto ownerOfTheRestaurant = getUserById(restaurantModel.getIdOwner(), tokenWithBearerPrefix);
        validateRoleOfTheUser(ownerOfTheRestaurant.getRol());
        return restaurantPersistencePort.saveRestaurant(restaurantModel);
    }

    private UserDto getUserById(Long idOwner, String tokenWithBearerPrefix) {
        return userGateway.getUserById(idOwner, tokenWithBearerPrefix);
    }

    private void validateRoleOfTheUser(String role) {
        if(!role.equals(ROLE_OWNER)) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }
}
